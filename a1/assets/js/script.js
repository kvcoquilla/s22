/*Mini-Activity*/
1. Create an a1 or "activity 1" folder and create an "users.json" file to store code translation of the activity.
	//Users
		id
		firstName
		lastName
		email
		password
		isAdmin
		mobileNumber

2. Create an "orders.json" to store oders data
	//Orders
		id
		userId
		transactionDate:
		status: (paid, not paid)
		total:
3. Create a "product.json" for product documents
	//Product
		id
		name
		description
		price
		stocks
		isActive
		sku
4. Create an "orderProducts.json"
	//Order Product
		orderId
		productId
		quantity
		price
		subTotal

Pushing Instructions:

    Go to Gitlab:
      -in your zuitt-projects folder and access b131 folder.
      -inside your b131 folder create a new folder/subgroup: s20
      -inside s22, create a new project/repo called activity
      -untick the readme option
      -copy the git url from the clone button of your activity repo.

    Go to Gitbash:
      -go to your b131/s22 folder and access activity folder
      -initialize activity folder as a local repo: git init
      -connect your local repo to our online repo: git remote add origin <gitURLOfOnlineRepo>
      -add your updates to be committed: git add .
      -commit your changes to be pushed: git commit -m "includes JSON activity 2"
      -push your updates to your online repo: git push origin master

    Go to Boodle:
      -copy the url of the home page for your s22/activity repo (URL on browser not the URL from clone button) and link it to boodle:

      WD078-22 | MongoDB - Data Modeling and Translation
